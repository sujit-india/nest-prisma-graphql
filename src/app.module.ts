import { Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { UsersModule } from "./users/users.module";
import { ApolloDriver } from "@nestjs/apollo";
import { ProductsModule } from "./products/products.module";
@Module({
  imports: [
    UsersModule,
    GraphQLModule.forRoot({
      typePaths: ["./**/*.graphql"],
      driver: ApolloDriver,
      uploads: false,
    }),
    ProductsModule,
  ],
})
export class AppModule {}
