import { Resolver, Query, Mutation, Args } from "@nestjs/graphql";
import { UsersService } from "./users.service";
import { RegisterInput, LoginInput } from "src/graphql";

@Resolver("User")
export class UserResolvers {
  constructor(private readonly userService: UsersService) {}

  @Query("getAllUsers")
  async getAllUsers() {
    return this.userService.getAllUsers();
  }

  @Mutation("register")
  async register(@Args("input") args: RegisterInput) {
    return this.userService.register(args);
  }

  @Mutation("login")
  async login(@Args("input") args: LoginInput) {
    return this.userService.login(args);
  }
}
