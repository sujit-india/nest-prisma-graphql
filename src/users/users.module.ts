import { Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { UserResolvers } from "./users.resolver";
import { PrismaService } from "../prisma.service";
import { JwtStrategy } from "src/strategies/jwt.strategy";
import { JwtModule } from "@nestjs/jwt";

@Module({
  imports: [JwtModule.register({ secret: process.env.JWT_SECRET })],
  providers: [UsersService, UserResolvers, PrismaService, JwtStrategy],
})
export class UsersModule {}
