import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { PrismaService } from "src/prisma.service";
import { User, RegisterInput, LoginInput, GetAllUsersPalyload } from "src/graphql";
import * as bcrypt from "bcryptjs";
import { JwtService } from "@nestjs/jwt";
import { message } from "src/common/customMessage";
@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService, private readonly jwt: JwtService) {}

  // get All Users
  async getAllUsers(): Promise<GetAllUsersPalyload[]> {
    return this.prisma.users.findMany({});
  }

  // Create a post
  async register(input: RegisterInput): Promise<User> {
    const { name, email, password } = input;
    const findUser = await this.prisma.users.findFirst({ where: { email } });

    if (findUser) throw new BadRequestException(message.USER_ALREADY_EXIST);

    const hash = bcrypt.hashSync(password, 10);

    const createUser = await this.prisma.users.create({ data: { name, email, hash } });
    const token = this.jwt.sign({ id: createUser.id, email });

    return { id: createUser.id, email, token, name };
  }

  // user Login
  async login(input: LoginInput): Promise<User> {
    const { email, password } = input;

    const findUserEmail = await this.prisma.users.findFirst({ where: { email } });
    if (!findUserEmail) throw new UnauthorizedException(message.INCORRECT_EMAIL_PASSWOED);
    if (!bcrypt.compareSync(password, findUserEmail.hash)) throw new UnauthorizedException(message.INCORRECT_EMAIL_PASSWOED);

    const token = this.jwt.sign({ id: findUserEmail.id, email });

    findUserEmail["token"] = token;

    return findUserEmail;
  }

  async validateUser(id: number) {
    return this.prisma.users.findFirst({ where: { id } });
  }
}
