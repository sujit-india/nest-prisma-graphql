import { Injectable } from "@nestjs/common";
import { AddProductInput, Product } from "src/graphql";
import { PrismaService } from "src/prisma.service";
import { S3Service } from "src/common/fileUpload";
import { constent } from "src/common/contants";
@Injectable()
export class ProductsService {
  constructor(private prisma: PrismaService, private readonly s3: S3Service) {}

  async addProduct(input: AddProductInput): Promise<Product> {
    const { name, price, image, category } = input;
    let uploadedObject;
    if (image && image.file) {
      uploadedObject = await this.s3.uploadFile(image.file, constent.productPath);
    }

    return await this.prisma.products.create({
      data: {
        name,
        price,
        category,
        image: uploadedObject ? uploadedObject.Key : "",
      },
    });
  }
}
