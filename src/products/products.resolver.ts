import { Args, Mutation, Resolver } from "@nestjs/graphql";
import { AddProductInput } from "src/graphql";
import { ProductsService } from "./products.service";

@Resolver("Product")
export class ProductsResolver {
  constructor(private readonly productsService: ProductsService) {}
  @Mutation("addProduct")
  async addProduct(@Args("input") args: AddProductInput) {
    return this.productsService.addProduct(args);
  }
}
