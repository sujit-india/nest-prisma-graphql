import { Module } from "@nestjs/common";
import { PrismaService } from "src/prisma.service";
import { ProductsResolver } from "./products.resolver";
import { ProductsService } from "./products.service";
import { S3Service } from "src/common/fileUpload";
@Module({
  providers: [ProductsService, ProductsResolver, PrismaService, S3Service],
})
export class ProductsModule {}
