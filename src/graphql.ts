
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class AddProductInput {
    name: string;
    price: number;
    category: string;
    image?: Nullable<Upload>;
}

export class RegisterInput {
    name: string;
    email: string;
    password: string;
}

export class LoginInput {
    email: string;
    password: string;
}

export class Product {
    id: number;
    name?: Nullable<string>;
    price?: Nullable<number>;
    category?: Nullable<string>;
    image?: Nullable<string>;
}

export abstract class IMutation {
    abstract addProduct(input: AddProductInput): Nullable<Product> | Promise<Nullable<Product>>;

    abstract register(input: RegisterInput): User | Promise<User>;

    abstract login(input: LoginInput): User | Promise<User>;
}

export class User {
    id: number;
    name?: Nullable<string>;
    email?: Nullable<string>;
    token?: Nullable<string>;
}

export class GetAllUsersPalyload {
    id: number;
    name?: Nullable<string>;
    email?: Nullable<string>;
}

export abstract class IQuery {
    abstract getAllUsers(): GetAllUsersPalyload[] | Promise<GetAllUsersPalyload[]>;
}

export type Upload = any;
type Nullable<T> = T | null;
