import { Injectable, InternalServerErrorException } from "@nestjs/common";
import * as AWS from "aws-sdk";
import { v4 as uuidv4 } from "uuid";
@Injectable()
export class S3Service {
  AWS_S3_BUCKET = process.env.AWS_S3_BUCKET;
  s3 = new AWS.S3({
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    secretAccessKey: process.env.AWS_S3_KEY_SECRET,
  });

  async uploadFile(file, key) {
    const { filename, createReadStream } = file;
    const fileExtention = filename.split(".")[1];
    const newFileName = uuidv4() + "." + fileExtention;
    const bufferData = await this.stream2buffer(createReadStream());
    return await this.s3_upload(bufferData, this.AWS_S3_BUCKET, `${key}/${newFileName}`, file.mimetype);
  }

  async s3_upload(file, bucket, name, mimetype) {
    const params = {
      Bucket: bucket,
      Key: name,
      Body: file,
      ACL: "public-read",
      ContentType: mimetype,
      ContentDisposition: "inline",
      CreateBucketConfiguration: {
        LocationConstraint: "ap-south-1",
      },
    };

    try {
      const s3Response = await this.s3.upload(params).promise();
      return s3Response;
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

  async stream2buffer(stream: any): Promise<Buffer> {
    return new Promise<Buffer>((resolve, reject) => {
      const _buf = Array<any>();

      stream.on("data", (chunk) => _buf.push(chunk));
      stream.on("end", () => resolve(Buffer.concat(_buf)));
      stream.on("error", (err) => reject(`error converting stream - ${err}`));
    });
  }
}
